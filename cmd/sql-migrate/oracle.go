// +build oracle

package main

import (
	_ "github.com/mattn/go-oci8"
	migrate "bitbucket.org/_metalogic_/sql-migrate"
)

func init() {
	dialects["oci8"] = migrate.OracleDialect{}
}
